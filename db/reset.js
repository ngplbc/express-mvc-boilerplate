const connection = require("./conf");

connection.query(`DROP TABLE IF EXISTS tablename`, 
  (err) => {
    if (err) {
      console.log(err.message);
    } else {
      connection.query(`CREATE TABLE tablename (
        id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
      );`, (err) => connection.end());
    }
  }
);

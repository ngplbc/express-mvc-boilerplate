const showHomePage = (req, res, next) => {
  return res.render("index");
};

module.exports = { showHomePage };
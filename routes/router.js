const express = require("express");
const router = express.Router();
const { showHomePage } = require("../controllers/page-controller")

router.get("/", showHomePage);

module.exports = router;
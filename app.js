const express = require("express");
const morgan = require("morgan");
const errorHandler = require("errorhandler");
// router
const router = require("./routes/router");

const app = express();
const PORT = 3000;

// middleware
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true}));
app.use(express.json());

// view engine
app.set("views", "./views");
app.set("view engine", "pug");

app.use("/", router);

app.use(errorHandler());

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));
